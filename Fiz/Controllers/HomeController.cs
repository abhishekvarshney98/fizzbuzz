﻿using Fiz.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Fiz.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;



        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(int Number)
        {
            if (Number < 1 || Number > 1000)
            {
                ViewBag.Number = string.Format("Invalid Value");
                 return View();
            }
            ViewBag.Number = string.Format("");
            var day = DateTime.Today.DayOfWeek;
            for (int i = 1; i <= Number; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    ViewBag.Number = string.Format("{0}FizzBuzz<br /> ", ViewBag.Number);
                }
                else if (i % 5 == 0)
                {
                    if (day.ToString() == "Tuesday")
                    {
                        ViewBag.Number = string.Format("{0}<span style='color:green;'>wuzz</span><br /> ", ViewBag.Number);
                    }
                    else
                    {
                        ViewBag.Number = string.Format("{0}<span style='color:green;'>Buzz</span><br /> ", ViewBag.Number);
                    }
                }
                else if (i % 3 == 0)
                {
                    if (day.ToString() == "Tuesday")
                    {
                        ViewBag.Number = string.Format("{0}<span style='color:blue;'>wizz</span><br /> ", ViewBag.Number);
                    }
                    else
                    {
                        ViewBag.Number = string.Format("{0}<span style='color:blue;'>Fizz</span><br /> ", ViewBag.Number);
                    }
                }
                else
                {
                    ViewBag.Number = string.Format("{0}{1} <br /> ", ViewBag.Number, i);
                }
            }
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
